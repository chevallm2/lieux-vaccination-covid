const fs = require('fs')

const unformmatedData = JSON.parse(fs.readFileSync('./data-unformatted.json', {encoding: 'utf-8'}))

const mappingFn = l => ({
    id: l.gid,
    adresseLieu: {
        id: l.id_adr,
        num: l.adr_num,
        voie: l.adr_voie,
        commune: {
            codePostal: l.com_cp,
            insee: l.com_insee,
            nom: l.com_nom
        },
        lat: l.lat_coor1,
        long: l.long_coor1,
        telephone: l.lieu_tel,
        accessible: new Boolean(l.lieu_accessibilite)
    },
    structure: {
        siren: l.structure_siren,
        raisonSociale: l.structure_rais,
        adresse: {
            id: l.id_adr,
            num: l.structure_num,
            voie: l.structure_voie,
            commune: {
                codePostal: l.structure_cp,
                insee: l.structure_insee,
                nom: l.structure_com
            },
        }
    },
    horaires: {
        disponible: new Boolean(l.rdv),
        lundi: l.rdv_lundi,
        mardi: l.rdv_mardi,
        mercredi: l.rdv_mercredi,
        jeudi: l.rdv_jeudi,
        vendredi: l.rdv_vendredi,
        samedi: l.rdv_samedi,
        dimanche: l.rdv_dimanche,
        debut: l.date_ouverture,
        fin: l.date_fermeture,
        telephone: l.rdv_tel,
        siteWeb: l.rdv_site_web,
        modalites: l.rdv_modalites,
        consultationPrevaccination: new Boolean(l.rdv_consultation_prevaccination)
    }
})

fs.writeFileSync('data.json', JSON.stringify(unformmatedData.map(mappingFn)))