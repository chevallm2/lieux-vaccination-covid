const map = L.map('map').setView([46.7122839, -0.4988385], 6);
const osmLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '© OpenStreetMap contributors',
    maxZoom: 19
});
    
map.addLayer(osmLayer);

const defaultIcon = L.icon({
    iconUrl: 'public/img/defaut.PNG',
    iconSize: [30,30]
})

const accessibleIcon = L.icon({
    iconUrl: 'public/img/accessible.PNG',
    iconSize: [30,30]
})

fetch('data.json')
    .then(data => data.json())
    .then(json => {
        const markers = json.map(lieu => {
            const adresse = `${lieu.adresseLieu.num} ${lieu.adresseLieu.voie} ${lieu.adresseLieu.commune.codePostal} ${lieu.adresseLieu.commune.nom}`
            const popupContent = `
            <div>
                <p><strong>${lieu.structure.raisonSociale}</strong></p>
                <em>${adresse}</em> <span title="Ce lieu est accessible aux PMR">${lieu.adresseLieu.accessible ? '♿': ''}</span>
                
                <p><strong>Horaires</strong></p>
                <table class="table">
                    <thead>
                        <th>Lundi</th>
                        <th>Mardi</th>
                        <th>Mercredi</th>
                        <th>Jeudi</th>
                        <th>Vendredi</th>
                        <th>Samedi</th>
                        <th>Dimanche</th>
                    </thead>
                    <tbody>
                        <td>${lieu.horaires.lundi}</td>
                        <td>${lieu.horaires.mardi}</td>
                        <td>${lieu.horaires.mercredi}</td>
                        <td>${lieu.horaires.jeudi}</td>
                        <td>${lieu.horaires.vendredi}</td>
                        <td>${lieu.horaires.samedi}</td>
                        <td>${lieu.horaires.dimanche}</td>
                    </tbody>
                </table>

                <p><strong>Prendre RDV</strong><p>
                <p>${lieu.horaires.telephone ? `Par téléphone au <a href="tel:+${lieu.horaires.telephone}">+${lieu.horaires.telephone}</a>` : ''}</p>
                <p>${lieu.horaires.siteWeb ? `Par internet sur <a href="${lieu.horaires.siteWeb}" target="_blank">ce lien</a>` : ''}</p>
            `
            const popup = L.popup({
                    className: 'popup'
                })
                .setContent(popupContent)
            return L.marker(
                [lieu.adresseLieu.lat, lieu.adresseLieu.long],
                {icon: lieu.adresseLieu.accessible ? accessibleIcon : defaultIcon}
                ).bindPopup(popup);
        }
        ).forEach(marker => marker.addTo(map))
        
    })